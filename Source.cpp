#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "Matrix.h"
using namespace std;

#if _MSC_VER
#define snprintf _snprintf_s
#endif

string itos(double i);
string getPath(const Matrix<double>& rates, const Matrix<int>& paths,string names[],unsigned from, unsigned to);
void FindArbitrage(Matrix<double>& stockMarket,string names[]);

int main (int argc, char** argv)
{
	if(argc != 2)
	{
		cerr << "Usage: <app> <currency exchange table file>";
		return 1;
	}

	string fileName = argv[1];
	//number of currencies
	const int NUM_CURRENCIES = 50;

	//setup names
	string names[50] = {
		"USD", "DZD", "ARS", "AUD", "BHD",
		"BOB", "BWP", "BRL", "GBP", "BND",
		"BGN", "CAD", "KYD", "XOF", "CLP",
		"CNY", "COP", "CRC", "HRK", "CZK",
		"DKK", "DOP", "EGP", "EEK", "EUR",
		"FJD", "HNL", "HKD", "HUF", "INR",
		"IDR", "ILS", "JMD", "JPY", "JOD",
		"KZT", "KES", "KWD", "LVL", "LBP",
		"LTL", "MKD", "MYR", "MUR", "MXN",
		"MDL", "MAD", "PHP", "SGD", "CHF"
	};

	ifstream input( fileName.c_str() );
	Matrix<double> stockMarket(NUM_CURRENCIES,NUM_CURRENCIES);

    // If the file could not be open, report an error and terminate.
    if( ! input ) {

        cerr << "Could not open: " << fileName << endl;
        return 1;
    }
    // Display the contents of the file once.
    string line;
	
    int lineNum = 0;
	
	//initialize matrix
    for(int i =0; i < NUM_CURRENCIES; i++){
		getline( input, line );
		stringstream ss(line);
		for(int j =0; j < NUM_CURRENCIES; j++)
		{
			ss >> stockMarket[i][j];
		}
	}

	if(NUM_CURRENCIES > 9)
	{
		cout << "Exchange rate table too big to display" << endl << endl;
	}
	else
	{
		//output initial table
		for(int i=0;i<NUM_CURRENCIES;i++)
		{
			for(int j=0;j<NUM_CURRENCIES;j++)
			{
				if(j != 0)
				{
					cout << '\t';
				}
				if(i == 0)
				{
					if(j == 0)
						continue;
					cout << names[j-1];
				}
				else if(j == 0)
				{
					cout << names[i-1];
				}
				else
				{
					cout << stockMarket[i-1][j-1];
				}
			}
			cout << endl;
		}
		cout << endl;
	}

	//find and display arbitrage
	FindArbitrage(stockMarket,names);
}

void FindArbitrage(Matrix<double>& stockMarket,string names[])
{
	// we have to keep two stages to make sure values are grabbed from non-current states only
	Matrix<double> stockMarket2(stockMarket.getWidth(),stockMarket.getHeight());
	Matrix<int> lastVert(stockMarket.getWidth(),stockMarket.getHeight()),lastVert2(stockMarket.getWidth(),stockMarket.getHeight());

	//keep a pointer to current and previous and rotate them to avoid copying
	Matrix<double> *currX = &stockMarket, *oldX;
	Matrix<int> *currVert = &lastVert, *oldVert;

	//initiliaze last vertex to non-existent (direct edge)
	for(unsigned i=0;i<stockMarket.getWidth();i++)
		for(unsigned j=0;j<stockMarket.getHeight();j++)
			lastVert[i][j] = -1;

	//main Floyd-Warshall loop
	for (unsigned k =0 ; k < stockMarket.getWidth() ; k++)
	{
		//rotate pointers
		if(currX = &stockMarket)
		{
			currX = &stockMarket2;
			currVert = &lastVert2;
			oldX = &stockMarket;
			oldVert = &lastVert;
		}
		else
		{
			currX = &stockMarket;
			currVert = &lastVert;
			oldX = &stockMarket2;
			oldVert = &lastVert2;
		}

		//check node distance
		for (unsigned i = 0 ; i < stockMarket.getWidth() ; i ++)
		{
			for ( unsigned j = 0 ; j < stockMarket.getWidth() ; j++)
			{
				// using  "less than"  as we are not inversing the weights 
				if ((*oldX)[i][j] < (*oldX)[i][k] * (*oldX)[k][j] )
				{
					//update weight and intermediate node
					(*currX)[i][j] = (*oldX)[i][k] * (*oldX)[k][j];
					(*currVert)[i][j] = k;
				}
				else
				{
					//update weight and intermediate node
					(*currX)[i][j] = (*oldX)[i][j];
					(*currVert)[i][j] = (*oldVert)[i][j];
				}
			}
		}
	}

	//storeMax
	int maxI,maxJ;
	double maxArb = 1;

	//output all arbitrage
	for (unsigned i = 0 ; i < stockMarket.getWidth() ; i ++)
	{
		for (unsigned j = i+1 ; j < stockMarket.getWidth() ; j++)
		{
			double arb = (*currX)[i][j]*(*currX)[j][i];
			//if product is greater, update maximal arbitrage
			if(arb > 1)
			{
				if(arb > maxArb)
				{
					maxI = i;
					maxJ = j;
					maxArb = arb;

					//reconstruct path
					string path = getPath(*currX,(*currVert),names,i,j)+string(" ")+getPath(*currX,(*currVert),names,j,i);
					cout << "One unit of " << names[i] << " will give " << arb << " units of the same currency on this path:" << endl << path << endl << endl;
				}
			}
		}
	}

	cout << "Maximal arbitrage:" << endl;
	
	//reconstruct path
	string path = getPath(*currX,(*currVert),names,maxI,maxJ)+string(" ")+getPath(*currX,(*currVert),names,maxJ,maxI);
	cout << "One unit of " << names[maxI] << " will give " << maxArb << " units of the same currency on this path:" << endl << path << endl << endl;
}

string getPath(const Matrix<double>& rates, const Matrix<int>& paths,string names[],unsigned from, unsigned to)
{
	int interm = paths[from][to];
	if(interm == -1)
		return names[from]+string("->")+names[to]+string("(")+itos(rates[from][to])+string(")");
	else
		return getPath(rates,paths,names,from,interm)+string(" ")+getPath(rates,paths,names,interm,to);
}

string itos(double i)
{
	char buffer[21];
    snprintf(buffer, 20, "%f", i);
    return string(buffer);
}