template <class T>
class Matrix
{
public:
    Matrix(unsigned _width,unsigned _height)
    {
        width = _width;
        height = _height;
        data = new T*[height];
        for(unsigned i=0;i<height;++i)
        {
            data[i] = new T[width];
        }
    }
    ~Matrix()
    {
        for(unsigned i=0;i<height;++i)
        {
            delete[] data[i];
        }
        delete[] data;
    }
	unsigned getWidth() const { return width; }
	unsigned getHeight() const { return width; }
    class MatrixRow
    {
    private:
        Matrix &parent;
        int y;
    public:
        MatrixRow(Matrix & mat, unsigned _y) : parent(mat), y(_y) {}
        T& operator[](unsigned x)
        {
            return parent.getElement(y,x);
        }
    };
	class ConstRow
    {
    private:
        const Matrix &parent;
        int y;
    public:
        ConstRow(const Matrix & mat, unsigned _y) : parent(mat), y(_y) {}
        T operator[](unsigned x) const
        {
            return parent.getElement(y,x);
        }
    };
    MatrixRow operator[] (unsigned y)
    {
        return MatrixRow(*this, y);
    }
    ConstRow operator[] (unsigned y) const
    {
        return ConstRow(*this, y);
    }
private:
    unsigned width, height;
    T** data;
    friend class MatrixRow;
    T& getElement(unsigned y,unsigned x) { return data[y][x]; }
    T getElement(unsigned y,unsigned x) const { return data[y][x]; }
};