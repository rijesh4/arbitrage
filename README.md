Currency Exchange Arbitrage:
A Mathematical Analysis
December 2013

1 Representing a Currency Exchange System as
a weighted directed multigraph
Given two currencies, i and j, let vi and vj denote vertices on a graph. Then,
construct ei;j , a directed edge from vi to vj with weight equal to the amount
of currency j you can buy with one unit of currency i. Let the function w(ei;j)
denote the weight of a single edge and the function w(fvi1 ; vi2 ; : : : ; ving) de-
note the combined weight of a path on the graph (going through the verices
vi1 ; vi2 ; : : : ; vin). Then, we can see that for any path, w(fvi1 ; vi2 ; : : : ; ving) =
w(ei1;i2 )w(ei2;i3 ): : :w(ein􀀀1;in). Let us assume that any currency is con-
vertible to every other currency. Then, for any two vertices, vi1 ; vi2 , the edge
w(ei1;i2 ) exists and there are always multiple paths from vi1 to vi2 . Finally,
let the function w(vi1 ; vi2 ) denote the minimum combined weight over all paths
on the graph between the vertices vi1 and vi2 , and respectively, w(vi1 ; vi2 ), the
maximum one.
The graph described above can be constructed in a program as an adjency
matrix of N  N values, where N is the number of currencies. Then, cell (i; j)
will simply be the value of ei;j . Any cell of the form (i; i) could simply equal 1,
since you can always get 1 unit of currency for 1 unit of the same currency.

2 Interpreting arbitrage over the graph
What creates an arbitrage? In simple terms, arbitrage would mean that if you
start o with 1 unit of a currency, you can, using only currency exchanges, end
up with more than one unit of that same currency. In other words, 9fvi1 ; vi2 ; : : : ; ving
such that w(fvi1 ; vi2 ; : : : ; vin; vi1g) > 1, or w(ei1;i2 )  : : :  w(ein;i1 ) > 1. How-
ever, if such a path exists, that automatically means that w(vi1 ; vi1 ) > 1, and,
for any intermediate vertex, vik , w(vi1 ; vik )  w(vik ; vi1 ) > 1
1

3 Finding an arbitrage cycle
As seen above, to nd an arbitrage cycle in our graph, all it takes is to nd two
nodes, vi1 ; vi2 such that w(vi1 ; vi2 )  w(vi2 ; vi1 ) > 1. So, if we create another
adjancy matrix for our nodes where each cell (i; j) will instead contain w(vi; vj ),
the problem reduces to nding a cell (i; j) such that the value in it times the
value in (j; i) is bigger than one.

4 Finding all-pairs maximal distances
A modied Floyd-Warshall algorithm can be used to nd all-pairs maximal
distances (Although Johnson's algorithm can perform the same task and it runs
in O(jV jjEj + jV j2 log jV j), that makes for no dierence in a complete graph,
but Floyd-Warshall's algorithm has the added benet of simplicity). Whereas
the original Floyd-Warshall algorithm compares the sums of distances between
nodes and updates if a smaller value is found, an algorithm can be created
based on the same idea that compares products of edge weights and updates if
a greater value is found. The basic idea will still be the same, starting with the
basic weights and checking at each step if adding an intermediate node would
increase the value. If at each update a last node matrix is updated where each
cell (i; j) contains the index of the intermediate node k that increased the value,
a path can recursively be reconstructed when needed. Using this approach, the
adjacency matrix with maximal distances above can be constructed, after which
all that's needed is a check for w(vi; vj)  w(vi; vj) > 1 and reconstruction of
the path. In total, Floyd-Warshall runs in O(N3), the maximal check runs in
O(N2) and the path reconstruction is in linear time. The complete algorithm
will thus be O(N3)

